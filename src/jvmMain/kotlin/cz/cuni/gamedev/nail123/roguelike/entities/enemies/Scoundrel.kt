package cz.cuni.gamedev.nail123.roguelike.entities.enemies

import cz.cuni.gamedev.nail123.roguelike.entities.attributes.HasSmell
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.HasVision
import cz.cuni.gamedev.nail123.roguelike.entities.items.Sword
import cz.cuni.gamedev.nail123.roguelike.mechanics.Pathfinding
import cz.cuni.gamedev.nail123.roguelike.mechanics.goBlindlyTowards
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles
import cz.cuni.gamedev.nail123.roguelike.world.worlds.HealthPotion
import kotlin.random.Random

class Scoundrel: Enemy(GameTiles.HUMAN), HasSmell, HasVision {
    override val blocksMovement = true
    override val blocksVision = false
    override val smellingRadius = 7
    override val visionRadius = 15

    override val maxHitpoints = 15
    override var hitpoints = 10
    override var attack = 3
    override var defense = 0

    override fun update() {
        if (Pathfinding.chebyshev(position, area.player.position) <= smellingRadius) {
            goBlindlyTowards(area.player.position)
        }
    }

    override fun die() {
        super.die()
        // Drop a sword
        if(Random.nextDouble() < 0.15) {
            this.block.entities.add(Sword(2))
        } else if (Random.nextDouble() < 0.30) {
            this.block.entities.add(HealthPotion())
        }
    }
}