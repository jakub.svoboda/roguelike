package cz.cuni.gamedev.nail123.roguelike.world.worlds;

import java.util.ArrayList;
import java.util.HashMap;

public class RatioEnemyRuleAction implements IRuleApplicationAction {
    private String enemySymbol;

    private ArrayList<EnemyType> enemies = new ArrayList<>();
    private ArrayList<Double> idealRatios = new ArrayList<>();

    private ArrayList<Integer> currentCounts = new ArrayList<>();
    private int totalExpanded = 0;

    public RatioEnemyRuleAction(String enemySymbol) {
        this.enemySymbol = enemySymbol;
    }

    public void addEnemy(EnemyType enemy, double ratio) {
        enemies.add(enemy);
        idealRatios.add(ratio);
        currentCounts.add(0);
    }

    @Override
    public void perform(Graph graph, RuleApplication application) {
        if(enemies.size() == 0) {
            return;
        }

        GraphGrammarRule rule = application.rule;
        HashMap<Integer, Integer> ruleToGraph = application.ruleNodeIdToGraphNodeId;
        rule.rhs.foreachNode(node -> {
            if(node.symbol == enemySymbol) {
                int graphId = ruleToGraph.get(node.id);
                Node graphNode = graph.nodes.get(graphId);
                EnemyType enemy = nextEnemy();
                switch(enemy) {
                    case RAT: {
                        graphNode.symbol = "rat";
                    }break;
                    case SCOUNDREL: {
                        graphNode.symbol = "scoundrel";
                    }break;
                }
            }
        });
    }

    private EnemyType nextEnemy() {
        int highestIndex = -1;
        double highestDifference = Double.NEGATIVE_INFINITY;
        for(int i = 0; i < idealRatios.size(); ++i) {
            double difference = idealRatios.get(i);
            if(totalExpanded != 0) {
                difference -= (double)currentCounts.get(i)/(double)totalExpanded;
            }
            if(highestDifference < difference) {
                highestDifference = difference;
                highestIndex = i;
            }
        }

        currentCounts.set(highestIndex, currentCounts.get(highestIndex) + 1);
        totalExpanded++;
        return enemies.get(highestIndex);
    }
}
