package cz.cuni.gamedev.nail123.roguelike.world.worlds;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

public class GraphGrammar {
    private class ProcessedRuleData {
        ArrayList<String> unknownSymbols = new ArrayList<>();
        int nonTerminalCount = 0;
    }

    public HashSet<String> nonTerminals = new HashSet<>();
    public HashSet<String> terminals = new HashSet<>();
    public ArrayList<GraphGrammarRule> rules = new ArrayList<>();
    public String startSymbol = "";

    public GraphGrammar() {
    }

    public boolean containsSymbol(String symbol) {
        return nonTerminals.contains(symbol) || terminals.contains(symbol);
    }

    public Result<Void> addNonTerminal(String symbol) {
        return addSymbol(nonTerminals, symbol);
    }

    public Result<Void> addTerminal(String symbol) {
        return addSymbol(terminals, symbol);
    }

    private Result<Void> addSymbol(HashSet<String> symbols, String symbol) {
        if(containsSymbol(symbol)) {
            return Result.error("Trying to add a duplicate symbol: " + symbol);
        }

        symbols.add(symbol);
        return Result.success();
    }

    public Result<Void> setStartSymbol(String start) {
        if(!nonTerminals.contains(start)) {
            return Result.error("Non-terminal '" + start + "' does not exist in the grammar, so it cannot be set as the starting symbol");
        }

        startSymbol = start;
        return Result.success();
    }

    public Result<Void> addRule(Graph lhs, Graph rhs) {
        return addRule(lhs, rhs, new ArrayList<>());
    }

    public Result<Void> addRule(Graph lhs, Graph rhs, IRuleApplicationAction ... actions) {
        return addRule(lhs, rhs, new ArrayList<>(Arrays.asList(actions)));
    }

    public Result<Void> addRule(Graph lhs, Graph rhs, ArrayList<IRuleApplicationAction> actions) {
        // action increases non terminal count or it adds an unknown symbol if it finds one
        ProcessedRuleData data = new ProcessedRuleData();
        IUnaryAction<Node> processRuleSide = node -> {
            if(nonTerminals.contains(node.symbol)) {
                data.nonTerminalCount++;
            }
            else if(!terminals.contains(node.symbol)) {
                data.unknownSymbols.add(node.symbol);
            }
        };

        // run the action on the LHS, if unknown symbols are found report error
        lhs.foreachNode(processRuleSide);
        if(data.unknownSymbols.size() > 0) {
            StringBuilder error = new StringBuilder("The LHS contains unknown symbols:");
            for (String symbol: data.unknownSymbols) {
                error.append(' ');
                error.append(symbol);
            }
            return Result.error(error.toString());
        }
        int lhsNonTerminalCount = data.nonTerminalCount;

        // run the action on the RHS, if unknown symbols are found report error
        data.nonTerminalCount = 0;
        rhs.foreachNode(processRuleSide);
        if(data.unknownSymbols.size() > 0) {
            StringBuilder error = new StringBuilder("The RHS contains unknown symbols:");
            for (String symbol: data.unknownSymbols) {
                error.append(' ');
                error.append(symbol);
            }
            return Result.error(error.toString());
        }
        int rhsNonTerminalCount = data.nonTerminalCount;

        // find the glue subgraph
        HashSet<Integer> glueNodes = new HashSet<>();
        lhs.foreachNode((node) -> {
            rhs.foreachNode((other) -> {
                if(node.id == other.id) {
                    glueNodes.add(node.id);
                }
            });
        });

        // todo:incomplete: we are not changing the edge type anywhere -js190620
        // Find out which edges between glue nodes in LHS need to be removed and which to add between glue nodes in RHS
        HashMap<Integer, Integer> glueEdgesToRemove = new HashMap<>();
        HashMap<Integer, Integer> glueEdgesToAdd = new HashMap<>();
        for(Integer glueNodeId: glueNodes) {
            Node lhsNode = lhs.nodes.get(glueNodeId);
            Node rhsNode = rhs.nodes.get(glueNodeId);

            // Find out the edges to add
            for(Edge rhsEdge: rhsNode.edges) {
                boolean edgeFound = false;
                for(Edge lhsEdge: lhsNode.edges) {
                    edgeFound = rhsEdge.equals(lhsEdge);
                    if(edgeFound) {
                        break;
                    }
                }
                if(!edgeFound) {
                    glueEdgesToAdd.put(rhsEdge.from.id, rhsEdge.to.id);
                }
            }

            // Find out the edges to remove
            for(Edge lhsEdge: lhsNode.edges) {
                boolean edgeFound = false;
                for(Edge rhsEdge: rhsNode.edges) {
                    edgeFound = lhsEdge.equals(rhsEdge);
                    if(edgeFound) {
                        break;
                    }
                }
                if(!edgeFound) {
                    glueEdgesToRemove.put(lhsEdge.from.id, lhsEdge.to.id);
                }
            }
        }

        // find out which nodes are to be removed and which nodes will keep the removed node edges
        HashMap<Integer,Integer> toRemoveToKeeping = new HashMap<>();
        HashSet<Integer> toRemoveWithoutKeeping = new HashSet<>();
        lhs.foreachNode((node) -> {
            if(!glueNodes.contains(node.id)) {
                ArrayList<Node> queue = new ArrayList<>(16);
                HashSet<Integer> visited = new HashSet<>();
                queue.add(node);
                for(int i = 0; i < queue.size(); ++i) {
                    Node current = queue.get(i);
                    if(glueNodes.contains(current.id)) {
                        toRemoveToKeeping.put(node.id, current.id);
                        break;
                    }
                    visited.add(current.id);
                    for(Edge incident: current.edges) {
                        Node neighbour = incident.getNeighbour(current);
                        if(incident.type == EdgeType.UNDIRECTED && !visited.contains(neighbour.id)) {
                                queue.add(neighbour);
                        }
                    }
                }
                toRemoveWithoutKeeping.add(node.id);
            }
        });

        // find out which nodes to add
        HashSet<Integer> nodesToAdd = new HashSet<>();
        rhs.foreachNode((rhsNode) -> {
            if(!glueNodes.contains(rhsNode.id)) {
                nodesToAdd.add(rhsNode.id);
            }
        });

        rules.add(new GraphGrammarRule(lhs, rhs, lhsNonTerminalCount, rhsNonTerminalCount, glueNodes, glueEdgesToRemove, glueEdgesToAdd, toRemoveToKeeping, toRemoveWithoutKeeping, nodesToAdd, actions));
        return Result.success();
    }
}
