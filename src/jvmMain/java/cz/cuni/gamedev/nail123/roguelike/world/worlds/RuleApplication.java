package cz.cuni.gamedev.nail123.roguelike.world.worlds;

import java.util.HashMap;

public class RuleApplication {
    public GraphGrammarRule rule;
    public HashMap<Integer, Integer> ruleNodeIdToGraphNodeId;

    public RuleApplication(GraphGrammarRule rule, HashMap<Integer, Integer> ruleNodeIdToGraphNodeId) {
        this.rule = rule;
        this.ruleNodeIdToGraphNodeId = ruleNodeIdToGraphNodeId;
    }
}
