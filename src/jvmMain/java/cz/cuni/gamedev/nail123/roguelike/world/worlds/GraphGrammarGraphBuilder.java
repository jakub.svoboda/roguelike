package cz.cuni.gamedev.nail123.roguelike.world.worlds;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;

public class GraphGrammarGraphBuilder {
    private GraphGrammar grammar;
    private Random random;

    private Graph mission;
    private int nodeCounter = 0;
    private int nonTerminalsCount = 0;
    private int maxNumberOfExpansions = 0;

    public GraphGrammarGraphBuilder(GraphGrammar grammar) {
        this(grammar, new Random().nextInt(), 1000);
    }

    public GraphGrammarGraphBuilder(GraphGrammar grammar, long seed, int maxNumberOfExpansions) {
        this.grammar = grammar;
        this.random = new Random(seed);
        this.maxNumberOfExpansions = maxNumberOfExpansions;
    }

    public Graph build() {
        mission = new Graph();
        nodeCounter = 0;

        addNode(grammar.startSymbol);
        nonTerminalsCount = 1;

        int numberOfIterations = 0;
        while(expand() && numberOfIterations < maxNumberOfExpansions) {
            numberOfIterations++;
        }
        if(nonTerminalsCount > 0) {
            expandWithFinalizingRules();
        }

        return mission;
    }

    private void expandWithFinalizingRules() {
        // todo:incomplete:
        throw new UnsupportedOperationException("expansion finalization is not supported yet");
    }

    private boolean expand() {
        // find applicable rules and their rule-to-graph mappings
        ArrayList<RuleApplication> ruleApplications = new ArrayList<>();
        for(GraphGrammarRule rule: grammar.rules) {
            Node lhsSomeNode = rule.lhs.nodes.entrySet().iterator().next().getValue();
            // todo:improvement: we could first check if the current graph even contains some of the symbols that are to be present -js190620
            mission.foreachNode((node) -> {
                if(node.symbol == lhsSomeNode.symbol) {
                    HashMap<Integer, Integer> lhsToGraph = new HashMap<>();
                    HashSet<Integer> lhsTraversed = new HashSet<>();
                    HashSet<Integer> graphTraversed= new HashSet<>();
                    boolean matched = tryMatching(lhsToGraph, lhsTraversed, graphTraversed, lhsSomeNode, node);
                    if(matched) {
                        ruleApplications.add(new RuleApplication(rule, lhsToGraph));
                    }
                }
            });
        }

        // todo:improvement: dead-ends could be checked when creating the grammar and propagated
        //  to the user with some friendly error message -js020620
        // todo:revise: what about cycles in the grammar? -js020620
        if(ruleApplications.size() == 0) {
            return false;
            //throw new GraphGrammarWorldException("Could not find applicable rule to the mission graph that still contains non-terminals!");
        }
        RuleApplication application = ruleApplications.get(random.nextInt(ruleApplications.size()));
        GraphGrammarRule rule = application.rule;
        HashMap<Integer, Integer> ruleToGraph = application.ruleNodeIdToGraphNodeId;

        // go through the nodes that are to be removed from the lhs and remove them from the graph
        for(HashMap.Entry<Integer,Integer> removeEntry: rule.nodesToRemoveToNodesKeepingEdges.entrySet()) {
            Integer lhsToRemoveId = removeEntry.getKey();
            Integer lhsKeeperId = removeEntry.getValue();

            Integer graphKeeperId = ruleToGraph.get(lhsKeeperId);

            // get the node to remove in the mission graph
            Integer graphToRemoveId = ruleToGraph.get(lhsToRemoveId);
            Node graphToRemove = mission.nodes.get(graphToRemoveId);

            // create new edges between to-remove neighbours and the keeper node
            for(Edge incident: graphToRemove.edges) {
                Node neighbour = incident.getNeighbour(graphToRemove);
                // we don't create, i.e. we remove, edges that are between the to-remove and the keeper
                if(neighbour.id == graphKeeperId) {
                    continue;
                }
                // try adding, if an identical edge already exists we don't care
                if(incident.from.id == graphToRemoveId) {
                    mission.tryAddEdge(graphKeeperId, neighbour.id);
                } else {
                    mission.tryAddEdge(neighbour.id, graphKeeperId);
                }
            }
            // remove the node and with it all its incident edges
            mission.removeNode(graphToRemoveId);
        }

        // there is no edges switching for these one, just simple remove
        for(Integer lhsToRemoveId: rule.toRemoveWithoutKeeping) {
            Integer graphToRemoveId = ruleToGraph.get(lhsToRemoveId);
            mission.removeNode(graphToRemoveId);
        }

        // Go through glue nodes and change symbols
        for(Integer glueNodeId: rule.glueNodes) {
            Integer graphNodeId = ruleToGraph.get(glueNodeId);
            Node graphNode = mission.nodes.get(graphNodeId);
            Node rhsNode = rule.rhs.nodes.get(glueNodeId);
            graphNode.symbol = rhsNode.symbol;
        }

        // Go through the nodes in RHS and add those that have to be added; only nodes
        for(Integer rhsNodeId: rule.nodesToAdd) {
            Node rhsNode = rule.rhs.nodes.get(rhsNodeId);
            int graphId = addNode(rhsNode.symbol);
            ruleToGraph.put(rhsNodeId, graphId);
        }


        // find possible parent for nodes that have none specified
        Node parent = null;
        boolean multipleParents = false;
        for(Integer glueNodeId: rule.glueNodes) {
            Node graphNode = mission.nodes.get(ruleToGraph.get(glueNodeId));
            if(graphNode.parent != null) {
                if(parent == null) {
                    parent = graphNode.parent;
                } else {
                    multipleParents = true;
                }
            }
        }

        // todo:incomplete: changing parents is not implemented -js190620
        // Go through the nodes in RHS again and add their edges and parents
        for(Integer rhsNodeId: rule.nodesToAdd) {
            Node rhsNode = rule.rhs.nodes.get(rhsNodeId);
            Integer graphNodeId = ruleToGraph.get(rhsNodeId);

            // Add to parent
            if(rhsNode.parent != null) {
                Integer parentNodeId = ruleToGraph.get(rhsNode.parent.id);
                mission.setNodeParent(graphNodeId, parentNodeId);
            } else if(!multipleParents) {
                if (parent != null) {
                    mission.setNodeParent(graphNodeId, parent.id);
                }
            } else {
                throw new UnsupportedOperationException("Adding a node without a parent specified when there is multiple possible parents is not supported!");
            }

            // Add edges
            for(Edge incident: rhsNode.edges) {
                Node rhsNeighbour = incident.getNeighbour(rhsNode);
                Integer graphNeighbourId = ruleToGraph.get(rhsNeighbour.id);
                if(incident.from.id == rhsNodeId) {
                    mission.tryAddEdge(graphNodeId, graphNeighbourId);
                } else {
                    mission.tryAddEdge(graphNeighbourId, graphNodeId);
                }
            }
        }

        // Remove glue nodes edges that are to be removed
        for(HashMap.Entry<Integer, Integer> entry: rule.glueNodesEdgesToRemove.entrySet()) {
            Integer graphFromId = ruleToGraph.get(entry.getKey());
            Integer graphToId = ruleToGraph.get(entry.getValue());
            mission.tryRemoveEdge(graphFromId, graphToId);
        }

        // Add  glue nodes edges that are to be added
        for(HashMap.Entry<Integer, Integer> entry: rule.glueNodesEdgesToAdd.entrySet()) {
            Integer graphFromId = ruleToGraph.get(entry.getKey());
            Integer graphToId = ruleToGraph.get(entry.getValue());
            mission.tryAddEdge(graphFromId, graphToId);
        }

        for(IRuleApplicationAction action: rule.actions) {
            action.perform(mission, application);
        }

        nonTerminalsCount += rule.rhsNonTerminalCount - rule.lhsNonTerminalCount;
        return true;
    }

    private boolean tryMatching(HashMap<Integer, Integer> lhsToGraph, HashSet<Integer> lhsTraversed, HashSet<Integer> graphTraversed, Node lhsNode, Node graphNode) {
        // todo:improvement: parallel edges are not supported -js050620
        // todo:incomplete: the lhsNode and graphNode should be removed from their corresponding sets if we don't match -js180620
        lhsTraversed.add(lhsNode.id);
        graphTraversed.add(graphNode.id);

        // try match parent
        if(lhsNode.parent != null) {
            if(graphNode.parent == null || lhsNode.parent.symbol != graphNode.parent.symbol) {
                return false;
            }

            // parent is not already matched or being matched, match it
            if(!lhsTraversed.contains(lhsNode.parent.id)) {
                if(!tryMatching(lhsToGraph, lhsTraversed, graphTraversed, lhsNode.parent, graphNode.parent)) {
                    return false;
                }
            }
        }

        // Find potential edge match candidates
        for(Edge lhsEdge: lhsNode.edges) {
            Node lhsNeighbour = lhsEdge.getNeighbour(lhsNode);
            if(lhsTraversed.contains(lhsNeighbour.id)) {
                continue;
            }

            ArrayList<Node> potentialMatches = new ArrayList<>();
            for(Edge graphEdge: graphNode.edges) {
                Node graphNeighbour = graphEdge.getNeighbour(graphNode);
                if(graphTraversed.contains(graphNeighbour.id)) {
                    continue;
                }
                if(graphNeighbour.symbol == lhsNeighbour.symbol) {
                    potentialMatches.add(graphNeighbour);
                }
            }

            // shuffle the array so that we traverse the matches randomly and thus adding randomness to where a rule is applied
            shuffleInPlace(potentialMatches);

            // try matching on the shuffled potential matches
            boolean matched = false;
            for(Node match: potentialMatches) {
                matched = tryMatching(lhsToGraph, lhsTraversed, graphTraversed, lhsNeighbour, match);
                if(matched) {
                    break;
                }
            }
            if(!matched) {
                return false;
            }
        }

        // find potential child match candidates
        for(Node lhsChild: lhsNode.children) {
            if(lhsTraversed.contains(lhsChild)) {
                continue;
            }
            ArrayList<Node> potentialMatches = new ArrayList<>();
            for(Node graphChild: graphNode.children) {
                if(graphTraversed.contains(graphChild)) {
                    continue;
                }
                if(graphChild.symbol == lhsChild.symbol) {
                    potentialMatches.add(graphChild);
                }
            }

            // shuffle the array so that we traverse the matches randomly and thus adding randomness to where a rule is applied
            shuffleInPlace(potentialMatches);

            // try matching on the shuffled potential matches
            boolean matched = false;
            for(Node match: potentialMatches) {
                matched = tryMatching(lhsToGraph, lhsTraversed, graphTraversed, lhsChild, match);
                if(matched) {
                    break;
                }
            }
            if(!matched) {
                return false;
            }
        }

        lhsToGraph.put(lhsNode.id, graphNode.id);
        return true;
    }

    private void shuffleInPlace(ArrayList<Node> potentialMatches) {
        for (int i = potentialMatches.size() - 1; i > 0; --i) {
            int randomIndex = random.nextInt(i + 1);
            Node temp = potentialMatches.get(i);
            potentialMatches.set(i, potentialMatches.get(randomIndex));
            potentialMatches.set(randomIndex, temp);
        }
    }

    private int addNode(String symbol) {
        int nodeId = nodeCounter++;
        mission.addNode(nodeId, symbol);
        return nodeId;
    }
}
