package cz.cuni.gamedev.nail123.roguelike.world.worlds;

import org.hexworks.zircon.api.data.Position3D;

import java.util.Objects;

public class Vector2 {
    public final static Vector2 zero = new Vector2(0,0);

    public final double x;
    public final double y;

    public Vector2() {
        x = 0;
        y = 0;
    }

    public Vector2(double  x, double y) {
        this.x = x;
        this.y = y;
    }

    public Vector2 add(Vector2 rhs) {
        return new Vector2(x + rhs.x, y + rhs.y);
    }

    public Vector2 negate() {
        return new Vector2(-x, -y);
    }

    public Vector2 minus(Vector2 rhs) {
        return new Vector2(x - rhs.x, y - rhs.y);
    }

    public Vector2 multiply(double scalar) {
        return new Vector2(x * scalar, y * scalar);
    }

    public Vector2 divide(double scalar) {
        return new Vector2(x / scalar, y / scalar);
    }

    public double dot(Vector2 rhs) {
        return x * rhs.x + y * rhs.y;
    }

    public double magnitude() {
        return Math.sqrt(x*x + y*y);
    }

    public Vector2 normalized() {
        return divide(magnitude());
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Vector2) {
            Vector2 other = (Vector2)obj;
            return x == other.x && y == other.y;
        }

        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public String toString() {
        return "[" + x + "," + y + "]";
    }
}
