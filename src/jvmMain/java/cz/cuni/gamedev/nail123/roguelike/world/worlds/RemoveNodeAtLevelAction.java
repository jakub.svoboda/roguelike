package cz.cuni.gamedev.nail123.roguelike.world.worlds;

public class RemoveNodeAtLevelAction implements IRuleApplicationAction {
    public int currentLevel = 0;
    private int dontSpawnAtLevel;
    private String shouldNotSpawnSymbol;

    public RemoveNodeAtLevelAction(String shouldNotSpawnSymbol, int dontSpawnAtLevel) {
        this.dontSpawnAtLevel = dontSpawnAtLevel;
        this.shouldNotSpawnSymbol = shouldNotSpawnSymbol;
    }

    @Override
    public void perform(Graph graph, RuleApplication application) {
        if(currentLevel != dontSpawnAtLevel) {
            return;
        }

        GraphGrammarRule rule = application.rule;
        rule.rhs.foreachNode(node -> {
            if(node.symbol == shouldNotSpawnSymbol) {
                int graphNodeId = application.ruleNodeIdToGraphNodeId.get(node.id);
                graph.removeNode(graphNodeId);
            }
        });
    }
}
