package cz.cuni.gamedev.nail123.roguelike.world.worlds;

import cz.cuni.gamedev.nail123.roguelike.blocks.Floor;
import cz.cuni.gamedev.nail123.roguelike.blocks.Wall;
import cz.cuni.gamedev.nail123.roguelike.entities.GameEntity;
import cz.cuni.gamedev.nail123.roguelike.entities.Player;
import cz.cuni.gamedev.nail123.roguelike.entities.enemies.Enemy;
import cz.cuni.gamedev.nail123.roguelike.entities.enemies.Rat;
import cz.cuni.gamedev.nail123.roguelike.entities.enemies.Scoundrel;
import cz.cuni.gamedev.nail123.roguelike.entities.items.Sword;
import cz.cuni.gamedev.nail123.roguelike.entities.objects.Stairs;
import cz.cuni.gamedev.nail123.roguelike.entities.unplacable.FogOfWar;
import cz.cuni.gamedev.nail123.roguelike.events.LoggedEvent;
import cz.cuni.gamedev.nail123.roguelike.mechanics.Pathfinding;
import cz.cuni.gamedev.nail123.roguelike.world.Area;
import cz.cuni.gamedev.nail123.roguelike.world.World;
import cz.cuni.gamedev.nail123.roguelike.world.builders.AreaBuilder;
import org.hexworks.zircon.api.data.Position3D;
import org.hexworks.zircon.api.data.Size3D;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;


/**
 * World generated using graph grammars. The generated graph represents a mission graph for the world.
 * The mission graph is laid out using the force-based graph drawing algorithm.
 */
public class GraphGrammarWorld extends World {
    int currentLevel = 0;
    GraphGrammarGraphBuilder missionGraphBuilder;
    GraphAreaBuilder builder;
    RatioEnemyRuleAction introAction;
    RatioEnemyRuleAction middleAction;
    RatioEnemyRuleAction finaleAction;
    RemoveNodeAtLevelAction removeEntranceAction;
    SetSwordPowerAction swordAction;

    public GraphGrammarWorld() {
    }

    private void handleError(Result result) {
        if(result.isFailure()) {
            logMessage(result.getError());
            throw new GraphGrammarWorldException(result.getError());
        }
    }

    private void initialize() {
        GraphGrammar missionGrammar = new GraphGrammar();

        // Setup symbols
        String nonTerminals[] = {"START", "CYCLE", "MIDDLE", "INTRO_ROOMS", "MIDDLE_ROOMS", "FINALE_ROOMS", "INTRO",
                "FINALE", "HALL_CONNECT", "BARRACKS_CONNECT", "CHECKPOINT_CONNECT", "KITCHEN_CONNECT", "COMMAND_CENTER",
                "CHECKPOINT", "ENTRANCE", "HALL", "OFFICE", "STORAGE", "ARMORY", "KITCHEN", "BARRACKS", "IN_CONNECT",
                "OUT_CONNECT", "SOME_ROOM", "IN", "OUT"};
        String terminals[] = {"command_center", "checkpoint", "entrance", "hall", "office", "storage", "armory",
                "kitchen", "barracks", "boss", "RatioEnemy", "intro", "middle", "finale", "stairs_up", "stairs_down",
                "player", "sword", "potion", "key", "lock", "cycle", "in", "out"};

        // add symbols
        for (String nonTerminal: nonTerminals) {
            handleError(missionGrammar.addNonTerminal(nonTerminal));
        }
        for (String terminal: terminals) {
            handleError(missionGrammar.addTerminal(terminal));
        }
        handleError(missionGrammar.setStartSymbol("START"));

        // initial rule linear
        {
            Graph lhs = new Graph();
            lhs.addNode(1, "START");
            Graph rhs = new Graph();
            rhs.addNode(1, "INTRO");
            rhs.addNode(2, "MIDDLE");
            rhs.addNode(3, "FINALE");
            rhs.addEdge(1, 2);
            rhs.addEdge(2, 3);
            handleError(missionGrammar.addRule(lhs, rhs));
        }

        // initial rule cycle
        {
            Graph lhs = new Graph();
            lhs.addNode(1, "START");
            Graph rhs = new Graph();
            rhs.addNode(1, "INTRO");
            rhs.addNode(2, "CYCLE");
            rhs.addNode(3, "FINALE");
            rhs.addNode(4, "CYCLE");
            rhs.addEdge(1, 2);
            rhs.addEdge(2, 3);
            rhs.addEdge(1, 4);
            rhs.addEdge(4, 3);
            handleError(missionGrammar.addRule(lhs, rhs));
        }

        // just simple linear
        {
            Graph lhs = new Graph();
            lhs.addNode(1, "CYCLE");
            Graph rhs = new Graph();
            rhs.addNode(1, "cycle");
            rhs.addNode(2, "IN_CONNECT");
            rhs.addNode(3, "OUT_CONNECT");
            rhs.addEdge(2, 3);
            rhs.setNodeParent(2, 1);
            rhs.setNodeParent(3, 1);
            handleError(missionGrammar.addRule(lhs, rhs));
        }

        // cycle in cycle
        {
            Graph lhs = new Graph();
            lhs.addNode(1, "CYCLE");
            Graph rhs = new Graph();
            rhs.addNode(1, "cycle");
            rhs.addNode(2, "IN_CONNECT");
            rhs.addNode(3, "SOME_ROOM");
            rhs.addNode(4, "SOME_ROOM");
            rhs.addNode(5, "OUT_CONNECT");
            rhs.addEdge(2, 3);
            rhs.addEdge(2, 4);
            rhs.addEdge(3, 5);
            rhs.addEdge(4, 5);
            rhs.setNodeParent(2, 1);
            rhs.setNodeParent(3, 1);
            rhs.setNodeParent(4, 1);
            rhs.setNodeParent(5, 1);
            handleError(missionGrammar.addRule(lhs, rhs));
        }

        // RESOLVE SOME_ROOM I
        {
            Graph lhs = new Graph();
            lhs.addNode(1, "SOME_ROOM");
            Graph rhs = new Graph();
            rhs.addNode(1, "ARMORY");
            handleError(missionGrammar.addRule(lhs, rhs));
        }
        {
            Graph lhs = new Graph();
            lhs.addNode(1, "SOME_ROOM");
            Graph rhs = new Graph();
            rhs.addNode(1, "KITCHEN");
            handleError(missionGrammar.addRule(lhs, rhs));
        }
        {
            Graph lhs = new Graph();
            lhs.addNode(1, "SOME_ROOM");
            Graph rhs = new Graph();
            rhs.addNode(1, "HALL");
            handleError(missionGrammar.addRule(lhs, rhs));
        }
        {
            Graph lhs = new Graph();
            lhs.addNode(1, "SOME_ROOM");
            Graph rhs = new Graph();
            rhs.addNode(1, "CHECKPOINT");
            handleError(missionGrammar.addRule(lhs, rhs));
        }
        {
            Graph lhs = new Graph();
            lhs.addNode(1, "SOME_ROOM");
            Graph rhs = new Graph();
            rhs.addNode(1, "BARRACKS");
            handleError(missionGrammar.addRule(lhs, rhs));
        }

        // intro next level
        {
            Graph lhs = new Graph();
            lhs.addNode(1, "INTRO");
            Graph rhs = new Graph();
            rhs.addNode(1, "intro");
            rhs.addNode(2, "INTRO_ROOMS");
            rhs.setNodeParent(2, 1);
            handleError(missionGrammar.addRule(lhs, rhs));
        }

        // middle next level
        {
            Graph lhs = new Graph();
            lhs.addNode(1, "MIDDLE");
            Graph rhs = new Graph();
            rhs.addNode(1, "middle");
            rhs.addNode(2, "MIDDLE_ROOMS");
            rhs.setNodeParent(2, 1);
            handleError(missionGrammar.addRule(lhs, rhs));
        }

        // finale next level
        {
            Graph lhs = new Graph();
            lhs.addNode(1, "FINALE");
            Graph rhs = new Graph();
            rhs.addNode(1, "finale");
            rhs.addNode(2, "FINALE_ROOMS");
            rhs.setNodeParent(2, 1);
            handleError(missionGrammar.addRule(lhs, rhs));
        }

        // intro rooms
        {
            Graph lhs = new Graph();
            lhs.addNode(1, "INTRO_ROOMS");
            Graph rhs = new Graph();
            rhs.addNode(1, "ENTRANCE");
            rhs.addNode(2, "HALL_CONNECT");
            rhs.addNode(3, "OFFICE");
            rhs.addNode(4, "STORAGE");
            rhs.addEdge(1, 2);
            rhs.addEdge(2, 3);
            rhs.addEdge(2, 4);
            handleError(missionGrammar.addRule(lhs, rhs));
        }

        // MIDDLE_ROOMS
        {
            Graph lhs = new Graph();
            lhs.addNode(1, "MIDDLE_ROOMS");
            Graph rhs = new Graph();
            rhs.addNode(1, "BARRACKS_CONNECT");
            rhs.addNode(2, "ARMORY");
            rhs.addNode(3, "KITCHEN_CONNECT");
            rhs.addEdge(1, 2);
            rhs.addEdge(2, 3);
            rhs.addEdge(3, 1);
            handleError(missionGrammar.addRule(lhs, rhs));
        }

        // FINALE_ROOMS
        {
            Graph lhs = new Graph();
            lhs.addNode(1, "FINALE_ROOMS");
            Graph rhs = new Graph();
            rhs.addNode(1, "CHECKPOINT_CONNECT");
            rhs.addNode(2, "COMMAND_CENTER");
            rhs.addEdge(1, 2);
            handleError(missionGrammar.addRule(lhs, rhs));
        }

        // Bridge checkpoint and barracks
        {
            Graph lhs = new Graph();
            lhs.addNode(1, "middle");
            lhs.addNode(2, "finale");
            lhs.addNode(3, "BARRACKS_CONNECT");
            lhs.addNode(4, "CHECKPOINT_CONNECT");
            lhs.setNodeParent(3, 1);
            lhs.setNodeParent(4, 2);
            lhs.addEdge(1, 2);

            Graph rhs = new Graph();
            rhs.addNode(1, "middle");
            rhs.addNode(2, "finale");
            rhs.addNode(3, "BARRACKS");
            rhs.addNode(4, "CHECKPOINT");
            rhs.addNode(5, "key");
            rhs.addNode(6, "lock");
            rhs.setNodeParent(3, 1);
            rhs.setNodeParent(4, 2);
            rhs.setNodeParent(5, 3);
            rhs.setNodeParent(6, 4);
            rhs.addEdge(1, 2);
            rhs.addEdge(3, 4);

            handleError(missionGrammar.addRule(lhs, rhs));
        }
        // Bridge hall and IN_CONNECT
        {
            Graph lhs = new Graph();
            lhs.addNode(1, "cycle");
            lhs.addNode(2, "finale");
            lhs.addNode(3, "OUT_CONNECT");
            lhs.addNode(4, "CHECKPOINT_CONNECT");
            lhs.setNodeParent(3, 1);
            lhs.setNodeParent(4, 2);
            lhs.addEdge(1, 2);

            Graph rhs = new Graph();
            rhs.addNode(1, "cycle");
            rhs.addNode(2, "finale");
            rhs.addNode(3, "OUT");
            rhs.addNode(4, "CHECKPOINT");
            rhs.setNodeParent(3, 1);
            rhs.setNodeParent(4, 2);
            rhs.addEdge(1, 2);
            rhs.addEdge(3, 4);

            handleError(missionGrammar.addRule(lhs, rhs));
        }
        // Bridge hall and IN_CONNECT
        {
            Graph lhs = new Graph();
            lhs.addNode(1, "cycle");
            lhs.addNode(2, "finale");
            lhs.addNode(3, "OUT_CONNECT");
            lhs.addNode(4, "CHECKPOINT");
            lhs.setNodeParent(3, 1);
            lhs.setNodeParent(4, 2);
            lhs.addEdge(1, 2);

            Graph rhs = new Graph();
            rhs.addNode(1, "cycle");
            rhs.addNode(2, "finale");
            rhs.addNode(3, "OUT");
            rhs.addNode(4, "CHECKPOINT");
            rhs.setNodeParent(3, 1);
            rhs.setNodeParent(4, 2);
            rhs.addEdge(1, 2);
            rhs.addEdge(3, 4);

            handleError(missionGrammar.addRule(lhs, rhs));
        }
        // Bridge hall and IN_CONNECT
        {
            Graph lhs = new Graph();
            lhs.addNode(1, "cycle");
            lhs.addNode(2, "finale");
            lhs.addNode(3, "OUT_CONNECT");
            lhs.addNode(4, "checkpoint");
            lhs.setNodeParent(3, 1);
            lhs.setNodeParent(4, 2);
            lhs.addEdge(1, 2);

            Graph rhs = new Graph();
            rhs.addNode(1, "cycle");
            rhs.addNode(2, "finale");
            rhs.addNode(3, "OUT");
            rhs.addNode(4, "checkpoint");
            rhs.setNodeParent(3, 1);
            rhs.setNodeParent(4, 2);
            rhs.addEdge(1, 2);
            rhs.addEdge(3, 4);

            handleError(missionGrammar.addRule(lhs, rhs));
        }

        // move key
        {
            Graph lhs = new Graph();
            lhs.addNode(1, "barracks");
            lhs.addNode(2, "key");
            lhs.setNodeParent(2, 1);
            lhs.addNode(3, "armory");
            lhs.addEdge(1, 3);

            Graph rhs = new Graph();
            rhs.addNode(1, "barracks");
            rhs.addNode(3, "armory");
            rhs.addNode(4, "key");
            rhs.setNodeParent(4, 3);
            rhs.addEdge(1, 3);

            handleError(missionGrammar.addRule(lhs, rhs));
        }
        // Bridge hall and IN_CONNECT
        {
            Graph lhs = new Graph();
            lhs.addNode(1, "intro");
            lhs.addNode(2, "cycle");
            lhs.addNode(3, "HALL_CONNECT");
            lhs.addNode(4, "IN_CONNECT");
            lhs.setNodeParent(3, 1);
            lhs.setNodeParent(4, 2);
            lhs.addEdge(1, 2);

            Graph rhs = new Graph();
            rhs.addNode(1, "intro");
            rhs.addNode(2, "cycle");
            rhs.addNode(3, "HALL");
            rhs.addNode(4, "IN");
            rhs.setNodeParent(3, 1);
            rhs.setNodeParent(4, 2);
            rhs.addEdge(1, 2);
            rhs.addEdge(3, 4);

            handleError(missionGrammar.addRule(lhs, rhs));
        }
        // Bridge hall and IN_CONNECT
        {
            Graph lhs = new Graph();
            lhs.addNode(1, "intro");
            lhs.addNode(2, "cycle");
            lhs.addNode(3, "HALL");
            lhs.addNode(4, "IN_CONNECT");
            lhs.setNodeParent(3, 1);
            lhs.setNodeParent(4, 2);
            lhs.addEdge(1, 2);

            Graph rhs = new Graph();
            rhs.addNode(1, "intro");
            rhs.addNode(2, "cycle");
            rhs.addNode(3, "HALL");
            rhs.addNode(4, "IN");
            rhs.setNodeParent(3, 1);
            rhs.setNodeParent(4, 2);
            rhs.addEdge(1, 2);
            rhs.addEdge(3, 4);

            handleError(missionGrammar.addRule(lhs, rhs));
        }
        // Bridge hall and IN_CONNECT
        {
            Graph lhs = new Graph();
            lhs.addNode(1, "intro");
            lhs.addNode(2, "cycle");
            lhs.addNode(3, "hall");
            lhs.addNode(4, "IN_CONNECT");
            lhs.setNodeParent(3, 1);
            lhs.setNodeParent(4, 2);
            lhs.addEdge(1, 2);

            Graph rhs = new Graph();
            rhs.addNode(1, "intro");
            rhs.addNode(2, "cycle");
            rhs.addNode(3, "hall");
            rhs.addNode(4, "IN");
            rhs.setNodeParent(3, 1);
            rhs.setNodeParent(4, 2);
            rhs.addEdge(1, 2);
            rhs.addEdge(3, 4);

            handleError(missionGrammar.addRule(lhs, rhs));
        }

        // Bridge hall and kitchen
        {
            Graph lhs = new Graph();
            lhs.addNode(1, "intro");
            lhs.addNode(2, "middle");
            lhs.addNode(3, "HALL_CONNECT");
            lhs.addNode(4, "KITCHEN_CONNECT");
            lhs.setNodeParent(3, 1);
            lhs.setNodeParent(4, 2);
            lhs.addEdge(1, 2);

            Graph rhs = new Graph();
            rhs.addNode(1, "intro");
            rhs.addNode(2, "middle");
            rhs.addNode(3, "HALL");
            rhs.addNode(4, "KITCHEN");
            rhs.setNodeParent(3, 1);
            rhs.setNodeParent(4, 2);
            rhs.addEdge(1, 2);
            rhs.addEdge(3, 4);

            handleError(missionGrammar.addRule(lhs, rhs));
        }

        // setup actions for the spawning of enemies
        {
            introAction = new RatioEnemyRuleAction("RatioEnemy");
            introAction.addEnemy(EnemyType.RAT, 0.7);
            introAction.addEnemy(EnemyType.SCOUNDREL, 0.3);

            middleAction = new RatioEnemyRuleAction("RatioEnemy");
            middleAction.addEnemy(EnemyType.RAT, 0.4);
            middleAction.addEnemy(EnemyType.SCOUNDREL, 0.6);

            finaleAction = new RatioEnemyRuleAction("RatioEnemy");
            finaleAction.addEnemy(EnemyType.RAT, 0.1);
            finaleAction.addEnemy(EnemyType.SCOUNDREL, 0.9);
        }

        {
            Graph lhs = new Graph();
            lhs.addNode(1, "COMMAND_CENTER");
            Graph rhs = new Graph();
            rhs.addNode(1, "command_center");
            rhs.addNode(2, "RatioEnemy");
            rhs.addNode(3, "stairs_down");
            rhs.setNodeParent(3, 1);
            rhs.setNodeParent(2, 1);
            handleError(missionGrammar.addRule(lhs, rhs, finaleAction));
        }
        {
            Graph lhs = new Graph();
            lhs.addNode(1, "CHECKPOINT");
            Graph rhs = new Graph();
            rhs.addNode(1, "checkpoint");
            rhs.addNode(2, "RatioEnemy");
            rhs.addNode(3, "potion");
            rhs.setNodeParent(2, 1);
            rhs.setNodeParent(3, 1);
            handleError(missionGrammar.addRule(lhs, rhs, finaleAction));
        }
        {
            Graph lhs = new Graph();
            lhs.addNode(1, "IN");
            Graph rhs = new Graph();
            rhs.addNode(1, "in");
            rhs.addNode(2, "RatioEnemy");
            rhs.setNodeParent(2, 1);
            handleError(missionGrammar.addRule(lhs, rhs, middleAction));
        }
        {
            Graph lhs = new Graph();
            lhs.addNode(1, "OUT");
            Graph rhs = new Graph();
            rhs.addNode(1, "out");
            rhs.addNode(2, "RatioEnemy");
            rhs.addNode(3, "potion");
            rhs.setNodeParent(2, 1);
            rhs.setNodeParent(3, 1);
            handleError(missionGrammar.addRule(lhs, rhs, middleAction));
        }
        {
            Graph lhs = new Graph();
            lhs.addNode(1, "ENTRANCE");
            Graph rhs = new Graph();
            rhs.addNode(1, "entrance");
            rhs.addNode(2, "player");
            rhs.addNode(3, "stairs_up");
            rhs.addNode(4, "potion");
            rhs.setNodeParent(2, 1);
            rhs.setNodeParent(3, 1);
            rhs.setNodeParent(4, 1);
            removeEntranceAction = new RemoveNodeAtLevelAction("stairs_up", 0);
            handleError(missionGrammar.addRule(lhs, rhs, removeEntranceAction));
        }
        {
            Graph lhs = new Graph();
            lhs.addNode(1, "HALL");
            Graph rhs = new Graph();
            rhs.addNode(1, "hall");
            rhs.addNode(2, "RatioEnemy");
            rhs.setNodeParent(2, 1);
            handleError(missionGrammar.addRule(lhs, rhs, introAction));
        }
        {
            Graph lhs = new Graph();
            lhs.addNode(1, "OFFICE");
            Graph rhs = new Graph();
            rhs.addNode(1, "office");
            rhs.addNode(2, "RatioEnemy");
            rhs.setNodeParent(2, 1);
            handleError(missionGrammar.addRule(lhs, rhs, introAction));
        }
        {
            Graph lhs = new Graph();
            lhs.addNode(1, "STORAGE");
            Graph rhs = new Graph();
            rhs.addNode(1, "storage");
            rhs.addNode(2, "RatioEnemy");
            rhs.addNode(3, "potion");
            rhs.setNodeParent(2, 1);
            rhs.setNodeParent(3, 1);
            handleError(missionGrammar.addRule(lhs, rhs, introAction));
        }
        {
            swordAction = new SetSwordPowerAction();
            Graph lhs = new Graph();
            lhs.addNode(1, "ARMORY");
            Graph rhs = new Graph();
            rhs.addNode(1, "armory");
            rhs.addNode(2, "RatioEnemy");
            rhs.addNode(3, "sword");
            rhs.setNodeParent(2, 1);
            rhs.setNodeParent(3, 1);
            handleError(missionGrammar.addRule(lhs, rhs, middleAction, swordAction));
        }
        {
            Graph lhs = new Graph();
            lhs.addNode(1, "KITCHEN");
            Graph rhs = new Graph();
            rhs.addNode(1, "kitchen");
            rhs.addNode(2, "RatioEnemy");
            rhs.addNode(3, "potion");
            rhs.setNodeParent(2, 1);
            rhs.setNodeParent(3, 1);
            handleError(missionGrammar.addRule(lhs, rhs, middleAction));
        }
        {
            Graph lhs = new Graph();
            lhs.addNode(1, "BARRACKS");
            Graph rhs = new Graph();
            rhs.addNode(1, "barracks");
            rhs.addNode(2, "RatioEnemy");
            rhs.setNodeParent(2, 1);
            handleError(missionGrammar.addRule(lhs, rhs, middleAction));
        }

        missionGraphBuilder = new GraphGrammarGraphBuilder(missionGrammar);
    }

    @NotNull
    @Override
    public Area buildStartingArea() {
        initialize();
        return buildLevel();
    }

    Area buildLevel() {
        removeEntranceAction.currentLevel = currentLevel;
        swordAction.swordPower = 4 + (currentLevel + 1) / 2;

        Graph missionGraph = missionGraphBuilder.build();

        ArrayList<Graph> roomsGraphs = missionGraph.getFlattenedGraphsOfDepth(1);
        if(roomsGraphs.size() != 1) {
            throw new RuntimeException("There should be only one room graph (i.e. rooms should be connected)!");
        }
        Graph roomsGraph = roomsGraphs.get(0);

        builder = (GraphAreaBuilder)(new GraphAreaBuilder(roomsGraph)).create();
        builder.addEntity(new FogOfWar(), Position3D.unknown());

        // spawn the entities (from the entity hierarchy level) of each room
        roomsGraph.foreachNode(room -> {
            Node missionRoom = missionGraph.nodes.get(room.id);
            for(Node child: missionRoom.children) {
                if(child.symbol == "lock") {
                    int threshold = (int) room.size + 1;
                    Position3D nodePosition = Position3DExtensions.fromVec(room.position);
                    int roomX = nodePosition.getX();
                    int roomY = nodePosition.getY();
                    for(Edge incident: room.edges) {
                        Node neighbour = incident.getNeighbour(room);
                        Position3D neighbourPos = Position3DExtensions.fromVec(neighbour.position);
                        Pathfinding.Result result = Pathfinding.INSTANCE.aStar(nodePosition, neighbourPos, builder, Pathfinding.INSTANCE.getFourDirectional(),
                                gameBlock -> false, Pathfinding.INSTANCE.getManhattan(), (position3D, position3D2) -> 1);

                        boolean performFromOtherSide = false;
                        for(Position3D pathStep: result.getPath()) {
                            int x = pathStep.getX();
                            int y = pathStep.getY();
                            if (x > roomX + threshold || x < roomX - threshold || y > roomY + threshold || y < roomY -threshold) {
                                if(builder.blocks.get(pathStep) instanceof Wall){
                                    performFromOtherSide = true;
                                    break;
                                }
                                builder.blocks.get(pathStep).getEntities().add(new LockedDoor());
                                break;
                            }
                        }
                        if(performFromOtherSide) {
                            int otherX = neighbourPos.getX();
                            int otherY = neighbourPos.getY();
                            int otherThreshold = (int) neighbour.size + 1;
                            Pathfinding.Result otherResult = Pathfinding.INSTANCE.aStar(neighbourPos, nodePosition, builder, Pathfinding.INSTANCE.getFourDirectional(),
                                    gameBlock -> false, Pathfinding.INSTANCE.getManhattan(), (position3D, position3D2) -> 1);
                            for(Position3D pathStep: otherResult.getPath()) {
                                int x = pathStep.getX();
                                int y = pathStep.getY();
                                if (x > otherX + otherThreshold || x < otherX - otherThreshold || y > otherY + otherThreshold || y < otherY - otherThreshold) {
                                    builder.blocks.get(pathStep).getEntities().add(new LockedDoor());
                                    break;
                                }
                            }
                        }
                        break;
                    }
                    continue;
                }
                GameEntity entity = getGameEntityFromNode(child);
                spawnInRoom(room, entity);
            }
        });

        // Build it into a full Area
        return builder.build();
    }

    @Nullable
    private GameEntity getGameEntityFromNode(Node node) {
        GameEntity entity = null;
        if(node.symbol == "rat") {
            entity = new Rat();
        } else if (node.symbol == "scoundrel") {
            entity = new Scoundrel();
        } else if(node.symbol == "potion") {
            entity = new HealthPotion();
        } else if(node.symbol == "stairs_up") {
            entity = new Stairs(false);
        } else if(node.symbol == "stairs_down") {
            entity = new Stairs(true);
        } else if(node.symbol == "player") {
            entity = builder.getPlayer();
        } else if(node.symbol.startsWith("sword")) {
            String powerSuffix = node.symbol.substring(5);
            int power = Integer.parseInt(powerSuffix);
            entity = new Sword(power);
        } else if(node.symbol == "key") {
            entity = new Key();
        }
        return entity;
    }

    private void logMessage(String message) {
        (new LoggedEvent(this, message)).emit();
    }

    public boolean spawnInRoom(Node node, GameEntity entity) {
        Position3D nodePosition = Position3DExtensions.fromVec(node.position);
        int radius = (int) node.size;
        int centerX = nodePosition.getX();
        int centerY = nodePosition.getY();

        Position3D roomOffset = Position3D.create(centerX - radius, centerY - radius, 0);
        Size3D roomSize = Size3D.create(2*radius + 1, 2* radius + 1, 1);
        boolean spawned = builder.addAtEmptyPosition(entity, roomOffset, roomSize);
        return spawned;
    }

    /**
     * Moving down - goes to a brand new level.
     */
    @Override
    public void moveDown() {
        ++currentLevel;
        logMessage("Descended to level " + (currentLevel + 1));
        if (currentLevel >= getAreas().getSize()) getAreas().add(buildLevel());
        goToArea(getAreas().get(currentLevel));
    }

    /**
     * Moving up would be for revisiting past levels, we do not need that. Check [DungeonWorld] for an implementation.
     */
    @Override
    public void moveUp() {
        // Not implemented
    }
}
