package cz.cuni.gamedev.nail123.roguelike.world.worlds;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class GraphGrammarRule {
    public Graph lhs;
    public Graph rhs;

    // todo:revise: these seem kind of out of place, plus the whole process of getting this in
    //  the GraphGrammar::addRule is not pretty, maybe this should be stored and/or calculated somewhere else?
    //  js200602
    public int lhsNonTerminalCount;
    public int rhsNonTerminalCount;
    /**
     * The IDs of the nodes that are the same in the LHS and RHS. Effectively these nodes
     * remain in the graph during expansion, with their symbol being possibly changed.
     */
    public HashSet<Integer> glueNodes;
    public HashMap<Integer, Integer> glueNodesEdgesToRemove;
    public HashMap<Integer, Integer> glueNodesEdgesToAdd;
    /**
     * LHS nodes that are to be removed from the matched graph will have an entry here.
     * The node is mapped to another node that is to take ownership of the edges of the removed node.
     */
    public HashMap<Integer, Integer> nodesToRemoveToNodesKeepingEdges;
    public HashSet<Integer> toRemoveWithoutKeeping;
    public HashSet<Integer> nodesToAdd;

    public ArrayList<IRuleApplicationAction> actions = null;

    public GraphGrammarRule(Graph lhs, Graph rhs, int lhsNonTerminalCount, int rhsNonTerminalCount,
                            HashSet<Integer> glueNodes, HashMap<Integer, Integer> glueNodesEdgesToRemove ,
                            HashMap<Integer, Integer> glueNodesEdgesToAdd, HashMap<Integer, Integer> nodesToRemoveToNodesKeepingEdges, HashSet<Integer> toRemoveWithoutKeeping,
                            HashSet<Integer> nodesToAdd, ArrayList<IRuleApplicationAction> actions) {
        this.lhs = lhs;
        this.rhs = rhs;
        this.lhsNonTerminalCount = lhsNonTerminalCount;
        this.rhsNonTerminalCount = rhsNonTerminalCount;
        this.glueNodes = glueNodes;
        this.glueNodesEdgesToRemove = glueNodesEdgesToRemove;
        this.glueNodesEdgesToAdd = glueNodesEdgesToAdd;
        this.nodesToRemoveToNodesKeepingEdges = nodesToRemoveToNodesKeepingEdges;
        this.nodesToAdd = nodesToAdd;
        this.actions = actions;
        this.toRemoveWithoutKeeping = toRemoveWithoutKeeping;
    }
}
