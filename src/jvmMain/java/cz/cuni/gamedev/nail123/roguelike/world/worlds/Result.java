package cz.cuni.gamedev.nail123.roguelike.world.worlds;

import com.sun.net.httpserver.Authenticator;

public class Result<T> {
    private boolean success = false;
    private boolean checked = false;
    private String error = "";
    private T value = null;
    private Result(T value) {
        this.success = true;
        this.value = value;
    }
    private Result(String error) {
        this.success = false;
        this.error = error;
    }

    public static <T> Result<T> create(boolean isSuccess, T value, String error) {
        if(isSuccess) {
            return Result.success(value);
        } else {
            return Result.error(error);
        }
    }

    public static <T> Result<T> success() {
        return success(null);
    }
    public static <T> Result<T> success(T value) {
        return new Result<T>(value);
    }
    public static <T> Result<T> error(String error) {
        return new Result<T>(error);
    }
    public boolean isSuccess() {
        checked = true;
        return success;
    }
    public boolean isFailure() {
        checked = true;
        return !success;
    }
    public T getValue() {
        if(!checked) {
            throw new RuntimeException("Trying to access a result's value without checking if the operation was a success or not!");
        }
        return value;
    }
    public String getError() {
        checked = true;
        return error;
    }
}
