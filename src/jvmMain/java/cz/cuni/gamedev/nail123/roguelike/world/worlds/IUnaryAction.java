package cz.cuni.gamedev.nail123.roguelike.world.worlds;

public interface IUnaryAction<T> {
    void perform(T parameter);
}
