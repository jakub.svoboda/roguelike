package cz.cuni.gamedev.nail123.roguelike.world.worlds;

import java.util.Random;

public class RandomExtensions {
    public static Vector2 nextInsideUnitCircle(Random random) {
        double t = 2f * Math.PI * random.nextDouble();
        double u = random.nextDouble() + random.nextDouble();
        double r = u > 1 ? 2 - u : u;
        return new Vector2(r * Math.cos(t), r * Math.sin(t));
    }
    public static double range(Random random, double min, double max) {
        double range = max - min;
        double value = random.nextDouble();
        return range * value + min;
    }
    public static int rangeInt(Random random, int min, int max) {
        int range = max - min;
        int value = random.nextInt(range + 1) + min;
        return value;
    }
}
