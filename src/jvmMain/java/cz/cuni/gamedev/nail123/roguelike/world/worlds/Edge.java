package cz.cuni.gamedev.nail123.roguelike.world.worlds;

public class Edge {
    public Node from;
    public Node to;
    public EdgeType type;

    public Edge(Node from, Node to) {
        this(from, to, EdgeType.UNDIRECTED);
    }

    public Edge(Node from, Node to, EdgeType type) {
        this.from = from;
        this.to = to;
        this.type = type;
    }

    public Node getNeighbour(Node node) {
        if(node == from) {
            return to;
        } else if(node == to) {
            return from;
        } else {
            throw new RuntimeException("Trying to get edge neighbour of a node that is not part in the edge");
        }
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Edge)) {
            return false;
        }
        Edge other = (Edge) obj;
        if(other.type != type) {
            return false;
        }
        if(type == EdgeType.UNDIRECTED || type == EdgeType.UNDIRECTED_FORBIDDEN) {
            return (from.equals(other.from) && to.equals(other.to)) || (from.equals(other.to) && to.equals(other.from));
        }

        throw new UnsupportedOperationException("Unsupported EdgeType comparison");
    }
}
