package cz.cuni.gamedev.nail123.roguelike.world.worlds;

import java.util.*;

public class ForceBasedGraphLayouter {
    public double repulsionStrength = 5;
    public double springStrength = 2;
    public double springLength = 3;
    public double timeStep = 0.1;
    public double timeScale = 0.5;
    public boolean stopAtGlobalMinimum = false;
    public double totalDistanceMovedMinimum = 0.001;
    public int maxNumberOfIterations = 600;
    public double minSize = 1;
    public double maxSize = 5;
    public double jitterStrength = 2.5;
    public int jitterForIterations = 400;

    private int width;
    private int height;
    private Random random;

    private boolean totalMoveMinimumReached = false;
    private int numberOfIterations;
    private Graph graph;

    public ForceBasedGraphLayouter(int width, int height) {
        this(width, height, new Random().nextInt(), 1000);
    }

    public ForceBasedGraphLayouter(int width ,int height, long seed, int maxNumberOfIterations) {
        this.width = width;
        this.height = height;
        this.random = new Random(seed);
        this.maxNumberOfIterations = maxNumberOfIterations;
    }

    // Update is called once per frame
    public void layout(Graph graph)  {
        initialize(graph);

        setInitialSizeAndPositions();
        while (!shouldStop()) {
            runSimulation();
            numberOfIterations++;
        }
    }

    private double clamp(double min, double max, double value) {
        if(value > max) {
            return max;
        } else if(value < min) {
            return min;
        } else {
            return value;
        }
    }
    private void runSimulation() {
        HashMap<Node, Vector2> nodeToForce = new HashMap<>();

        // calculate forces
        double totalDistanceMoved = 0.0;
        for(Map.Entry<Integer, Node> entry: graph.nodes.entrySet()) {
            Node node = entry.getValue();

            // calculate repulsion between each vertex
            Vector2 repulsion = Vector2.zero;
            for(Map.Entry<Integer, Node> otherEntry: graph.nodes.entrySet()) {
                Node other = otherEntry.getValue();
                if(node == other) {
                    continue;
                }

                Vector2 otherToNode = node.position.minus(other.position);
                double distance = otherToNode.magnitude();
                Vector2 direction = otherToNode.divide(distance);
                repulsion = repulsion.add(direction.multiply(repulsionStrength * node.size * node.size *
                        other.size * other.size / (distance * distance)));
            }

            Vector2 spring = Vector2.zero;
            for(Edge incident: node.edges) {
                Node neighbour = incident.getNeighbour(node);
                Vector2 nodeToNeighbour = neighbour.position.minus(node.position);
                double distance = nodeToNeighbour.magnitude();
                Vector2 direction = nodeToNeighbour.divide(distance);

                spring = spring.add(direction.multiply(springStrength * (distance - node.size - neighbour.size - springLength)));
            }
            Vector2 force = repulsion.add(spring);
            nodeToForce.put(node, force);
        }

        // apply forces and bound to the area
        for(Map.Entry<Node, Vector2> entry: nodeToForce.entrySet()) {
            Node node = entry.getKey();
            Vector2 force = entry.getValue();

            // apply force
            Vector2 oldPosition = node.position;
            Vector2 jitter = new Vector2(
                    RandomExtensions.range(random, -jitterStrength, jitterStrength),
                    RandomExtensions.range(random, -jitterStrength, jitterStrength));
            jitter =  jitter.multiply(clamp(0, 1, 1.0 - (numberOfIterations / jitterForIterations)));
            node.position = node.position.add(force.add(jitter).multiply(timeScale * timeStep));

            // bound the position
            double maxX = width - node.size;
            double minX = node.size;
            double maxY = height - node.size;
            double minY = node.size;
            if (node.position.x > maxX) {
                node.position = new Vector2(maxX, node.position.y);
            } else if (node.position.x < minX) {
                node.position = new Vector2(minX, node.position.y);
            }
            if (node.position.y > maxY) {
                node.position = new Vector2(node.position.x, maxY);
            } else if (node.position.y < minY) {
                node.position = new Vector2(node.position.x, minY);
            }

            totalDistanceMoved += node.position.minus(oldPosition).magnitude();
        }

        totalMoveMinimumReached = (totalDistanceMoved < totalDistanceMovedMinimum);
    }

    private boolean shouldStop() {
        if (stopAtGlobalMinimum) {
            return totalMoveMinimumReached;
        }

        return numberOfIterations >= maxNumberOfIterations;
    }

    private void initialize(Graph graph) {
        totalMoveMinimumReached = false;
        numberOfIterations = 0;
        this.graph = graph;
    }

    private void setInitialSizeAndPositions() {
        graph.foreachNode((node) -> {
            node.size = RandomExtensions.range(random, minSize, maxSize);

            int size = (int)Math.ceil(node.size);
            int x = RandomExtensions.rangeInt(random, size, width - size);
            int y = random.nextInt(height - 2*size) + size;
            node.position = new Vector2(x, y);
        });
    }
}
