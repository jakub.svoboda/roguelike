package cz.cuni.gamedev.nail123.roguelike.world.worlds;

public enum EdgeType {
    UNDIRECTED,
    UNDIRECTED_FORBIDDEN
}
