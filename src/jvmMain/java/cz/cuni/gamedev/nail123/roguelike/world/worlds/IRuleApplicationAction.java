package cz.cuni.gamedev.nail123.roguelike.world.worlds;

public interface IRuleApplicationAction {
    void perform(Graph graph, RuleApplication application);
}
