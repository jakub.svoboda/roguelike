package cz.cuni.gamedev.nail123.roguelike.world.worlds;

public class GraphGrammarWorldException extends RuntimeException {
    public GraphGrammarWorldException(String message) {
        super(message);
    }
}
