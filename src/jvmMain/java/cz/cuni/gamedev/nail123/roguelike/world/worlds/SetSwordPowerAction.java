package cz.cuni.gamedev.nail123.roguelike.world.worlds;

public class SetSwordPowerAction implements  IRuleApplicationAction {
    int swordPower = 4;

    @Override
    public void perform(Graph graph, RuleApplication application) {
        GraphGrammarRule rule = application.rule;
        rule.rhs.foreachNode(node -> {
            if(node.symbol == "sword") {
                int graphNodeId = application.ruleNodeIdToGraphNodeId.get(node.id);
                graph.nodes.get(graphNodeId).symbol = "sword" + swordPower;
            }
        });
    }
}
