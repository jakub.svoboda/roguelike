package cz.cuni.gamedev.nail123.roguelike.world.worlds;


import java.util.*;

public class Graph {
    public HashMap<Integer, Node> nodes = new HashMap<>();
    public HashMap<Integer, Edge> edges = new HashMap<>();

    private void throwOnError(Result<Void> result) {
        if(result.isFailure()) {
            throw new RuntimeException(result.getError());
        }
    }

    public void addNode(int nodeId, String nodeSymbol) {
        throwOnError(tryAddNode(nodeId,nodeSymbol));
    }

    public Result<Void> tryAddNode(int nodeId, String nodeSymbol) {
        Node node = nodes.put(nodeId, new Node(nodeId, nodeSymbol));
        return Result.create(node == null, null, "Trying to add node with duplicate id " + nodeId);
    }

    public void removeNode(int nodeId) {
        throwOnError(tryRemoveNode(nodeId));
    }

    public Result<Void> tryRemoveNode(int nodeId) {
        Result<Node> find = findNode(nodeId);
        if(find.isFailure()) {
            return Result.error("The node " + nodeId + " does not exist in the graph");
        }

        ArrayList<Integer> edgesToRemove = new ArrayList<>();
        for(Map.Entry<Integer, Edge> entry: edges.entrySet()) {
            Edge edge = entry.getValue();
            if(edge.from.id == nodeId || edge.to.id == nodeId) {
                edgesToRemove.add(entry.getKey());
                edge.from.edges.remove(edge);
                edge.to.edges.remove(edge);
            }
        }

        for(Integer key: edgesToRemove) {
            edges.remove(key);
        }

        Node node = find.getValue();
        if(node.parent != null) {
            node.parent.children.remove(node);
            for(Node child: node.children) {
                node.parent.children.add(child);
            }
        }
        for(Node child: node.children) {
            child.parent = node.parent;
            child.depth -= 1;
        }

        nodes.remove(nodeId);
        return Result.success();
    }

    public void addEdge(int fromId, int toId) {
        throwOnError(tryAddEdge(fromId, toId, EdgeType.UNDIRECTED));
    }
    public void addEdge(int fromId, int toId, EdgeType type) {
        throwOnError(tryAddEdge(fromId, toId, type));
    }

    public Result<Void> tryAddEdge(int fromId, int toId) {
        return tryAddEdge(fromId, toId, EdgeType.UNDIRECTED);
    }

    public Result<Void> tryAddEdge(int fromId, int toId, EdgeType type) {
        Integer edgeId = createEdgeId(fromId, toId);
        return tryAddEdge(edgeId, fromId, toId, type);
    }

    private Integer createEdgeId(int fromId, int toId) {
        return Objects.hash(fromId, toId);
    }

    public Result<Void> tryAddEdge(int edgeId, int fromId, int toId, EdgeType type) {
        if(edges.containsKey(edgeId)) {
            return Result.error("Graph already contains an edge from " + fromId + " to " + toId + " with id: " + toId);
        }

        Result<Node> fromResult = findNode(fromId);
        if(fromResult.isFailure()) {
            return Result.error(fromResult.getError());
        }
        Result<Node> toResult = findNode(toId);
        if(toResult.isFailure()) {
            return Result.error(toResult.getError());
        }
        Node fromNode = fromResult.getValue();
        Node toNode = toResult.getValue();
        Edge edge = new Edge(fromNode, toNode);
        fromNode.edges.add(edge);
        toNode.edges.add(edge);

        edges.put(edgeId, edge);

        return Result.success();
    }

    public Result<Node> findNode(int id) {
        Node node = nodes.get(id);
        return Result.create(node != null, node, "Node with id " + id + " is not present");
    }

    public void setNodeParent(int nodeId, int parentId) {
        throwOnError(trySetNodeParent(nodeId, parentId));
    }

    private Result<Void> trySetNodeParent(int nodeId, int parentId) {
        Result<Node> nodeResult = findNode(nodeId);
        if(nodeResult.isFailure()) {
            return Result.error(nodeResult.getError());
        }
        Result<Node> parentResult = findNode(parentId);
        if(parentResult.isFailure()) {
            return Result.error(parentResult.getError());
        }

        Node node = nodeResult.getValue();
        Node parent = parentResult.getValue();
        if(node.parent != null) {
            node.parent.children.remove(node);
        }
        node.parent = parent;
        parent.children.add(node);
        node.depth = parent.depth + 1;
        increaseDepth(node.children, 1);

        return Result.success();
    }

    private void increaseDepth(ArrayList<Node> nodes, int amount) {
        for (Node node: nodes) {
            node.depth += amount;
            increaseDepth(node.children, amount);
        }
    }

    public ArrayList<Graph> getFlattenedGraphsOfDepth(int depth) {
        ArrayList<Graph> result = new ArrayList<>();
        HashSet<Integer> nodesAdded = new HashSet<>();
        for(Map.Entry<Integer, Node> entry: nodes.entrySet()) {
            Node node = entry.getValue();
            if(node.depth != depth || nodesAdded.contains(node.id)) {
                continue;
            }

            Graph graph = new Graph();
            ArrayList<Integer> queue = new ArrayList<>();
            graph.addNode(node.id, node.symbol);
            queue.add(node.id);
            nodesAdded.add(node.id);
            for(int i = 0; i < queue.size(); ++i) {
                Integer currentId = queue.get(i);
                Node current = nodes.get(currentId);
                for(Edge incident: current.edges) {
                    Node neighbour = incident.getNeighbour(current);
                    if(!nodesAdded.contains(neighbour.id)) {
                        graph.addNode(neighbour.id, neighbour.symbol);
                        queue.add(neighbour.id);
                        nodesAdded.add(neighbour.id);
                    }
                    if(current == incident.from) {
                        graph.addEdge(currentId, neighbour.id, incident.type);
                    }
                }
            }
            result.add(graph);
        }

        return result;
    }

    public void foreachNode(IUnaryAction<Node> action) {
        for(Map.Entry<Integer, Node> entry: nodes.entrySet()) {
            Node node = entry.getValue();
            action.perform(node);
        }
    }

    public void foreachEdge(IUnaryAction<Edge> action) {
        for(Map.Entry<Integer, Edge> entry: edges.entrySet()) {
            Edge edge = entry.getValue();
            action.perform(edge);
        }
    }

    public Result<Void> tryRemoveEdge(Integer fromId, Integer toId) {
        Integer edgeId = createEdgeId(fromId, toId);
        if(!edges.containsKey(edgeId)) {
            return Result.error("Edge to be removed is not present in the graph!");
        }

        edges.remove(edgeId);
        return Result.success();
    }
}

