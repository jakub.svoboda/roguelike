package cz.cuni.gamedev.nail123.roguelike.world.worlds;

import cz.cuni.gamedev.nail123.roguelike.entities.attributes.HasInventory;
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.Inventory;
import cz.cuni.gamedev.nail123.roguelike.entities.items.Item;
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles;
import org.jetbrains.annotations.NotNull;

public class Key extends Item {
    public Key() {
        super(GameTiles.INSTANCE.getKEY());
    }
    @NotNull
    @Override
    public Inventory.EquipResult isEquipable(@NotNull HasInventory character) {
        return new Inventory.EquipResult(false, "");
    }

    @Override
    protected void onEquip(@NotNull HasInventory character) {

    }

    @Override
    protected void onUnequip(@NotNull HasInventory character) {

    }
}
