package cz.cuni.gamedev.nail123.roguelike.world.worlds;

import org.hexworks.zircon.api.data.Position3D;

public class Position3DExtensions {
    public static Position3D fromVec(Vector2 vector) {
        return Position3D.create((int) vector.x, (int) vector.y, 0);
    }
}
