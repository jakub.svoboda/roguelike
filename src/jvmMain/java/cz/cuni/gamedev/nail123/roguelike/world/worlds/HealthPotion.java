package cz.cuni.gamedev.nail123.roguelike.world.worlds;

import com.github.jroskopf.wfc.model.data.Tile;
import cz.cuni.gamedev.nail123.roguelike.entities.Player;
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.HasInventory;
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.Inventory;
import cz.cuni.gamedev.nail123.roguelike.entities.items.Item;
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles;
import org.jetbrains.annotations.NotNull;

public class HealthPotion extends Item {
    int healingPower;

    public HealthPotion() {
        this(5);
    }
    public HealthPotion(int healingPower) {
        super(GameTiles.INSTANCE.getPOTION());
    }

    @NotNull
    @Override
    public Inventory.EquipResult isEquipable(@NotNull HasInventory character) {
        return new Inventory.EquipResult(character.getItems().contains(this), "");
    }

    @Override
    protected void onEquip(@NotNull HasInventory character) {
        if(character instanceof Player) {
            Player player = (Player) character;
            player.setHitpoints(player.getHitpoints() + 5);
        }
        character.getInventory().remove(this);
    }

    @Override
    protected void onUnequip(@NotNull HasInventory character) {

    }
}
