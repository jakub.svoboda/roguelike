package cz.cuni.gamedev.nail123.roguelike.world.worlds;

import java.util.Objects;

public class Pair<X, Y> {
    public final X a;
    public final Y b;
    public Pair(X a, Y b) {
        this.a = a;
        this.b = b;
    }

    public static <X, Y> Pair<X, Y> make(X a, Y b) {
        return new Pair<>(a, b);
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Pair) {
            Pair other = (Pair) obj;
            return a.equals(other.a) && b.equals(other.b);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(a, b);
    }
}
