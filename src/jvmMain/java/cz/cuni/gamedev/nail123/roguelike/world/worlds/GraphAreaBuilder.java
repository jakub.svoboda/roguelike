package cz.cuni.gamedev.nail123.roguelike.world.worlds;

import cz.cuni.gamedev.nail123.roguelike.GameConfig;
import cz.cuni.gamedev.nail123.roguelike.blocks.Floor;
import cz.cuni.gamedev.nail123.roguelike.blocks.GameBlock;
import cz.cuni.gamedev.nail123.roguelike.blocks.Wall;
import cz.cuni.gamedev.nail123.roguelike.mechanics.Pathfinding;
import cz.cuni.gamedev.nail123.roguelike.world.builders.AreaBuilder;
import cz.cuni.gamedev.nail123.utils.collections.ObservableMap;
import org.hexworks.zircon.api.data.Position3D;
import org.hexworks.zircon.api.data.Size3D;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;

public class GraphAreaBuilder extends AreaBuilder {
    public ObservableMap<Position3D, GameBlock> blocks;
    public Size3D size;
    public int width;
    public int height;
    public Graph roomsGraph;

    public GraphAreaBuilder(Graph roomsGraph) {
        super(GameConfig.INSTANCE.getVISIBLE_SIZE(), GameConfig.INSTANCE.getVISIBLE_SIZE());
        this.blocks = getBlocks();
        this.size = getVisibleSize();
        this.width = size.getXLength();
        this.height = size.getYLength();
        this.roomsGraph = roomsGraph;
    }
    @NotNull
    @Override
    public AreaBuilder create() {
        ForceBasedGraphLayouter layouter = new ForceBasedGraphLayouter(size.getXLength() - 1, size.getYLength() - 1, (new Random()).nextLong(), 5000);
        layouter.layout(roomsGraph);

        // Every tile is wall by default
        for (int x = 0; x < width; ++x) {
            for (int y = 0; y < height; ++y) {
                blocks.put(Position3D.create(x, y, 0), new Wall());
            }
        }

        // Build rooms
        roomsGraph.foreachNode(node -> {
            buildRoom(node);
        });

        // Connect rooms
        HashSet<Edge> connected = new HashSet<>();
        for(Map.Entry<Integer,Node> nodeEntry: roomsGraph.nodes.entrySet()) {
            Node node = nodeEntry.getValue();
            Position3D nodePosition = Position3DExtensions.fromVec(node.position);
            for(Edge incident: node.edges) {
                if(connected.contains(incident)) {
                    continue;
                }
                Node neighbour = incident.getNeighbour(node);
                Position3D neighbourPos = Position3DExtensions.fromVec(neighbour.position);
                Pathfinding.Result result = Pathfinding.INSTANCE.aStar(nodePosition, neighbourPos, this, Pathfinding.INSTANCE.getFourDirectional(),
                        gameBlock -> false, Pathfinding.INSTANCE.getManhattan(), (position3D, position3D2) -> 1);
                for(Position3D pathStep: result.getPath()) {
                    if(blocks.get(pathStep) instanceof Wall) {
                        blocks.put(pathStep, new Floor());
                    }
                }
                connected.add(incident);
            }
        }

        return this;
    }

    public void buildRoom(Node node) {
        int roomSize = (int) node.size;
        Position3D position = Position3DExtensions.fromVec(node.position);
        int centerX = position.getX();
        int centerY = position.getY();
        for(int x = centerX - roomSize; x <= centerX + roomSize; ++x) {
            for(int y = centerY - roomSize; y <= centerY + roomSize; ++y) {
                boolean isBorder = (x == 0 || x == width - 1 || y == 0 || y == height - 1);
                if(isBorder) {
                    continue;
                }

                blocks.put(Position3D.create(x, y, 0), new Floor());
            }
        }
    }
}
