package cz.cuni.gamedev.nail123.roguelike.world.worlds;

import java.util.ArrayList;

public class Node {
    public int id;
    public ArrayList<Edge> edges = new ArrayList<>();
    public int depth = 0;
    public Node parent = null;
    public ArrayList<Node> children = new ArrayList<>();

    // todo:revise: the position is kinda tacked on, the Node is more of a general purpose and
    //  the position is only relevant for layout and space generation, so the Node could be
    //  wrapped there or just have some hashmap for the positions I guess. Similiar goes for the symbol.
    //  js200602
    public String symbol;
    public Vector2 position = new Vector2(0, 0);
    public double size = 0;

    public Node(int id, String symbol) {
        this.id = id;
        this.symbol = symbol;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof  Node) {
            Node other = (Node) obj;
            return id == other.id;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Integer.hashCode(id);
    }
}
